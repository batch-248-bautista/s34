const express = require("express");

const app = express();
const port = 3000;

app.use(express.json()) 
app.use(express.urlencoded({extended:true}))

//1
app.get("/home",(req, res)=>{
	res.send("Hello to the homepage.")
})

//2
let users = [
{
	username: "johndoe",
	password:"johndoe1234"
}
]
app.get("/users",(req, res)=>{
	res.send(users)
})


//3
app.delete("/delete-user",(req, res)=>{
	//creates a avriable to store themessage to be sent back postman
	let message;
	let indexNum = -1;
	//creates a for loop that will loop throguh the elemnts of the "users" array
	for(let i=0; i<users.length; i++){
		if(req.body.username === users[i].username){
			indexNum = i;
			break;
		}
		if (indexNum === -1){
			message = `User ${req.body.username} does not exist.`;
		}
		else{
			users.splice(indexNum, 1);
			message = `User ${req.body.username} has been deleted.`;
		}
	}
	res.send(message);

})

// app.delete("/delete-user",(req, res)=>{
// 	let index = users.findIndex(item => item.password === req.query.password);
// 	users.splice(index, 1);
// 	res.send(users);
// })

app.listen(port,()=> console.log(`Server running at port ${port}`))