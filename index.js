//express package ws importes as express
const express = require("express");

//invoked express package to create a server or PI and save it in avriable which we can refer to later to create routes

const app = express();//server

//variable for port assignment
const port = 4000;

//express.json() is a method form express that allws us to handel the stream of data from our client and receive the data and automaticallu prase the incoming JSON form our request.

//app.use() is a method to run function or method for our expres..js api
//it use to run middlewares

app.use(express.json()) //method for us to run anohter function or method inside of it

//but applying the option of extended:true this allows us to receive information in other dat types such as an object which we will use throughout our application.
app.use(express.urlencoded({extended:true}))

//CREATEA ROUTE IN EXPRESS- express has methos corresponding to each HTTP Method
app.get("/",(req, res)=>{
	//res.send uses the express JS Module's method instead to send a response bak to the client.
	res.send("Hello from our default Express.js GET route!")
})

//miniAct
app.post("/",(req, res)=>{
	//res.send uses the express JS Module's method instead to send a response bak to the client.
	res.send(`Hello there, ${req.body.firstName} ${req.body.lastName}!`)
})


app.put("/",(req, res)=>{
	//res.send uses the express JS Module's method instead to send a response bak to the client.
	res.send("Hello from our default Express.js PUT route!")
})

app.delete("/",(req, res)=>{
	//res.send uses the express JS Module's method instead to send a response bak to the client.
	res.send("Hello from our default Express.js DELETE route!")
})

//ad mock database

let users = [
{
	username: "cardo_dalisay",
	password:"quiapo"
},
{
	username: "mommy_d",
	password:"nardadarna"
},
{
	username: "kagawad_godbless",
	password:"dingangbato"
}
]

app.post('/signup',(req,res)=>{
	console.log(req.body)
	//request/req.body contains the body of the request or the input
	if (req.body.username !== '' && req.body.password !== ''){
		users.push(req.body)
		res.send(users)
	}
	else{
		res.send("Pleaseinput BOTH username and password.")
	}
	
	
})

//thi expects to recieev a put request at a URI "/change-password"

app.put("/change-password",(req, res)=>{
	//creates a avriable to store themessage to be sent back postman
	let message;
	//creates a for loop that will loop throguh the elemnts of the "users" array
	for(let i=0; i<users.length; i++){
		if(req.body.username == users[i].username){
			users[i].password = req.body.password;
			message = `User ${req.body.username}'s password has been updated!`;
			break;
		}
		else{
			message= "User does not exist!";
		}
	}
	res.send(message)

})
//tells our server to listen to the port 
app.listen(port,()=> console.log(`Server running at port ${port}`))